//
//  XGAnnotationView.h
//  XGMapRelatedOperations
//
//  Created by 小果 on 2016/11/21.
//  Copyright © 2016年 小果. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface XGAnnotationView : MKAnnotationView
+(instancetype)annotationWithMapView:(MKMapView *)mapView;
@end
